var arr;
var ev = 0;
var i = 0;
var b;
var mas = []
var sas = []
var pageNav= 0;
var pageNavNext;

let fullPage = document.querySelector(".slider__link_full")
fullPage.addEventListener("click",FullDisplay)
let magazine = window.location.search.replace( '?', '');

async function GetApi(magazine) {

    if(!magazine){
        setTimeout( 'location="https://abreview.ru/archive/";', 4000 );
    }else if(magazine){
        let requestURL = `https://abreview.ru/magazine/api/?${magazine}`;
        fetch(requestURL, {method: 'GET'})
            .then(response => response.json())
            .then(data => {

                arr = data;
                ShowData(data.ITEMS);
                SliderShow();

            })
            .catch(error => {
                    setTimeout( 'location="https://abreview.ru/404.php";', 2000 );

            });
    }



}

function ShowData(data) {

    let z = 0;
    let pageNav = 0
    if (document.documentElement.clientWidth > 912){
        for(let y = 1;y<(data.length/2)+1;y++){
                let mainSlider = document.querySelector("#mainCarousel")
                let sample = mainSlider.querySelector("#doubleImg").content.cloneNode(true);
                let imgSlider = sample.querySelectorAll('.imgdouble');

                if(data.length%2 != 0){
                    if(z===0){
                        imgSlider[0].src = " ";
                        imgSlider[1].src = data[z].DETAIL;
                        z = z+1;
                        imgSlider[0].classList.add("slider__slide_visible")
                    }else if (z>=data.length-2){
                        imgSlider[0].src =data[data.length - 2].DETAIL;
                        imgSlider[1].src = data[data.length - 1].DETAIL;
                    }else if (z>0){
                        imgSlider[0].src = data[z].DETAIL;
                        imgSlider[1].src =data[z+1].DETAIL;
                        z = z + 2;
                    }

                }else if(data.length%2 === 0){
                    if(z===0){
                        imgSlider[0].src = " ";
                        imgSlider[1].src = data[z].DETAIL;
                        z = z+1;
                        imgSlider[0].classList.add("slider__slide_visible")
                    }else if(data.length-1===z){
                        imgSlider[0].src =data[data.length - 1].DETAIL;
                        imgSlider[1].src = " ";

                        imgSlider[1].classList.add("slider__slide_visible")
                    }else if(data.length-1>z){
                        imgSlider[0].src =data[data.length - 1].DETAIL;
                        imgSlider[1].src = " ";
                        imgSlider[1].classList.add("slider__slide_visible")
                    }else if(z>0){
                        imgSlider[0].src = data[z].DETAIL;
                        imgSlider[1].src =data[z+1].DETAIL;
                        z = z + 2;
                    }
                }
                    sas.push(sample);

                   sas.forEach((el)=>{
                    mainSlider.appendChild(el);

                })

        }
       for(let x = 1;x<(data.length/2)+1;x++){

        let navSlider = document.querySelector("#navCarousel")
        let sampleNav = navSlider.querySelector("#sampleNav").content.cloneNode(true);
        let imgNav = sampleNav.querySelectorAll('.imgdouble');

           if(data.length%2 != 0){
               if(pageNav===0){
                   imgNav[0].src = " ";
                   imgNav[1].src = data[pageNav].DETAIL;
                   pageNav = pageNav+1;
                   imgNav[0].classList.add("slider__slide_visible")
               }else if (pageNav>=data.length-2){
                   imgNav[0].src =data[data.length - 2].DETAIL;
                   imgNav[1].src = data[data.length - 1].DETAIL;
               }else if (pageNav>0){
                   imgNav[0].src = data[pageNav].DETAIL;
                   imgNav[1].src =data[pageNav+1].DETAIL;
                   pageNav = pageNav + 2;
               }

           }else if(data.length%2 === 0){
               if(pageNav===0){
                   imgNav[0].src = " ";
                   imgNav[1].src = data[pageNav].DETAIL;

                   pageNav = pageNav+1;
                   imgNav[0].classList.add("slider__slide_visible")
               }else if(data.length-1===pageNav){
                   imgNav[0].src =data[data.length - 1].DETAIL;
                   imgNav[1].src = " ";
                   imgNav[1].classList.add("slider__slide_visible")

               }else if(data.length-1>pageNav){
                   imgNav[0].src =data[data.length - 1].DETAIL;
                   imgNav[1].src = " ";
                   imgNav[1].classList.add("slider__slide_visible")

               }else if(pageNav>0){
                   imgNav[0].src = data[pageNav].DETAIL;
                   imgNav[1].src =data[pageNav+1].DETAIL;

                   pageNav = pageNav + 2;
               }
           }


            mas.push(sampleNav);


           mas.forEach((el)=>{
            navSlider.appendChild(el);

        })
       }

    }else{
        data.forEach((el) => {
            magazinNew(el.DETAIL);
        })
    }

}

function SliderShow() {
    const mainCarousel = new Carousel(document.querySelector("#mainCarousel"), {
        Dots: false,
        slidesPerPage: 1,
        infinite: false,
        center: true,
        fill: false,
        on: {

            init: (carousel,event,mainCarousel) => {

                carousel.$index = document.querySelectorAll(".carousel_index");
                carousel.$doubleIndex = document.querySelectorAll(".carousel_double_index")
                carousel.$count = document.querySelectorAll(".carousel_count");

            },

            createSlide: (carousel, slide) => {
                slide.Panzoom = new Panzoom(slide.$el.querySelector(".panzoom"), {
                    panOnlyZoomed: true,
                    resizeParent: true,
                    zoomFriction:0,
                });
            },


            refresh: (carousel) => {

                if (carousel.$count) {
                    if (document.documentElement.clientWidth > 912) {
                        carousel.$count[0].innerHTML = arr.ITEMS.length;
                        carousel.$count[1].innerHTML = arr.ITEMS.length;
                        carousel.$index[0].innerHTML = arr.ITEMS[i].NAME
                        carousel.$index[1].innerHTML = arr.ITEMS[i].NAME
                    }else {
                        carousel.$count[0].innerHTML = arr.ITEMS.length;
                        carousel.$count[1].innerHTML = arr.ITEMS.length;
                        carousel.$index[0].innerHTML = arr.ITEMS[i].NAME
                    }
                }

            },


            change: (carousel, event) => {


                if (document.documentElement.clientWidth > 912) {
                    ShowPage(carousel, event);
                }else{
                     changePageMobile(carousel, event)
                }
                document.addEventListener("click",function (){
                    i=0;
                    carousel.slideTo(Number[1])
                })


            },
            deleteSlide: (carousel, slide) => {
                if (slide.Panzoom) {
                    slide.Panzoom.destroy();
                    slide.Panzoom = null;
                }
            },

        },


    })

    if (document.documentElement.clientWidth > 912) {

    const navCarousel = new Carousel(document.querySelector("#navCarousel"), {
        Sync: {
            target: mainCarousel,
        },
        Dots: false,
        center: true,
        infinite: false,
        fill: false,
        slidesPerPage: 1,
        on: {
            init: (carousel,event) => {
               carousel.$index = document.querySelectorAll(".carousel_index");
               carousel.$doubleIndex = document.querySelectorAll(".carousel_double_index")
                carousel.$count = document.querySelectorAll(".carousel_count");

            },
          change: (carousel,event) => {

              NavShowPage(carousel, event)
            },

        },

    });
    }
}

function FullDisplay(){
    let elem = document.documentElement;
    if (elem.requestFullscreen) {
        elem.requestFullscreen();
    } else if (elem.webkitRequestFullscreen) {
        elem.webkitRequestFullscreen();
    } else if (elem.msRequestFullscreen) {
        elem.msRequestFullscreen();
    }

    if (document.exitFullscreen) {
        document.exitFullscreen();
    } else if (document.webkitExitFullscreen) {
        document.webkitExitFullscreen();
    } else if (document.msExitFullscreen) {
        document.msExitFullscreen();
    }

}

function magazinNew(el) {

    let mainSlider = document.querySelector("#mainCarousel")
    let sampleSlider = mainSlider.querySelector("#sample").content.cloneNode(true);
    let fancyDiv = sampleSlider.querySelector('.slider__slide')
    let imgSlider = sampleSlider.querySelector('.img');
    let imgSrc = el;
    fancyDiv.setAttribute("data-src", el);
    imgSlider.src = imgSrc;
    mainSlider.appendChild(sampleSlider);

}

function NavShowPage(carousel, event){

    if(arr.ITEMS.length%2===0){

        if (ev < event) {

            if(arr.ITEMS.length-2 === i){
                i= arr.ITEMS.length-2
                b =  arr.ITEMS.length-1
                pageNav = i
                pageNavNext=b
                carousel.$index[0].innerHTML = arr.ITEMS[i].NAME;
                carousel.$doubleIndex[0].innerHTML = " "

            } else  if(arr.ITEMS.length-1 === i ) {
                i++
                b = i + 1
                pageNav = i
                pageNavNext=b
                carousel.$index[0].innerHTML = arr.ITEMS[i].NAME;
                carousel.$doubleIndex[0].innerHTML = " "
            }else  if( i === 0){

                i++
                b = i + 1
                pageNav = i
                pageNavNext=b
                carousel.$index[0].innerHTML = arr.ITEMS[i].NAME;
                carousel.$doubleIndex[0].innerHTML = arr.ITEMS[b].NAME
            }else if(arr.ITEMS.length-1 > i ){
                i=arr.ITEMS.length-1
                b = arr.ITEMS.length-1
                pageNav = i
                pageNavNext=b
                carousel.$index[0].innerHTML = arr.ITEMS[i].NAME;
                carousel.$doubleIndex[0].innerHTML = " "
            }else if(arr.ITEMS.length-2> i > 0) {
                i = i + 2;
                b = i + 1
                pageNav = i
                pageNavNext=b
                carousel.$index[0].innerHTML = arr.ITEMS[i].NAME;
                carousel.$doubleIndex[0].innerHTML = arr.ITEMS[b].NAME


            }


        }
        if (ev > event) {
            i = i - 2
            b = i + 1
            pageNav = i
            pageNavNext=b
            if (i < 0) {
                i = 0;
                pageNav = i
                pageNavNext=b
                carousel.$index[0].innerHTML = arr.ITEMS[i].NAME;
                carousel.$doubleIndex[0].innerHTML = " ";


            } else {
                carousel.$index[0].innerHTML = arr.ITEMS[i].NAME;
                carousel.$doubleIndex[0].innerHTML = arr.ITEMS[b].NAME


            }
        }
        if (ev === event){
            pageNav = i
            pageNavNext=b
            carousel.$index[0].innerHTML =  carousel.$index[1].innerHTML;
            carousel.$doubleIndex[0].innerHTML = carousel.$doubleIndex[1].innerHTML

        }

    }else if(arr.ITEMS.length%2!=0){

        if (ev < event) {

            if(arr.ITEMS.length-2 ===  pageNav){
                pageNav= arr.ITEMS.length-2
                pageNavNext =  arr.ITEMS.length-1
                carousel.$index[1].innerHTML = arr.ITEMS[pageNav].NAME;
                carousel.$doubleIndex[1].innerHTML = arr.ITEMS[ pageNavNext].NAME


            }else  if(arr.ITEMS.length-1 ===  pageNav ) {
                pageNav=arr.ITEMS.length-2
                pageNavNext = arr.ITEMS.length-1
                carousel.$index[1].innerHTML = arr.ITEMS[ pageNav].NAME;
                carousel.$doubleIndex[1].innerHTML = arr.ITEMS[ pageNavNext].NAME
            }else  if( pageNav === 0){

                pageNav++
                pageNavNext= pageNav + 1
                carousel.$index[1].innerHTML = arr.ITEMS[pageNav].NAME;
                carousel.$doubleIndex[1].innerHTML = arr.ITEMS[pageNavNext].NAME
            }else if(arr.ITEMS.length-1 > pageNav ){
                pageNav=arr.ITEMS.length-2
                pageNavNext = arr.ITEMS.length-1

                carousel.$index[1].innerHTML = arr.ITEMS[pageNav].NAME;
                carousel.$doubleIndex[1].innerHTML = arr.ITEMS[pageNavNext].NAME;
            }else if(arr.ITEMS.length-2> pageNav > 0) {
                pageNav = pageNav + 2;
                pageNavNext = pageNav + 1

                carousel.$index[1].innerHTML = arr.ITEMS[pageNav].NAME;
                carousel.$doubleIndex[1].innerHTML = arr.ITEMS[pageNavNext].NAME
            }


        }
        if (ev > event) {
            pageNav = pageNav - 2
            pageNavNext = pageNav + 1
            if (pageNav <= 0) {
                pageNav = 0;
                carousel.$index[1].innerHTML = arr.ITEMS[pageNav].NAME;
                carousel.$doubleIndex[1].innerHTML = " ";

            } else {
                carousel.$index[1].innerHTML = arr.ITEMS[pageNav].NAME;
                carousel.$doubleIndex[1].innerHTML = arr.ITEMS[pageNavNext].NAME
            }
        }
        if (ev === event){
            pageNav=i
         pageNavNext=b
         carousel.$index[1].innerHTML=  carousel.$index[0].innerHTML
         carousel.$doubleIndex[1].innerHTML=carousel.$doubleIndex[0].innerHTML
        }
    }

ev=event;

}

function ShowPage(carousel, event) {

    if(arr.ITEMS.length%2===0){

        if (carousel.prevPage < carousel.pageIndex) {

            if(arr.ITEMS.length-2 === i){
                i= arr.ITEMS.length-2
                b =  arr.ITEMS.length-1
                pageNav = i
                pageNavNext=b
                carousel.$index[0].innerHTML = arr.ITEMS[i].NAME;
                carousel.$doubleIndex[0].innerHTML = " "

            } else  if(arr.ITEMS.length-1 === i ) {
                i++
                b = i + 1
                pageNav = i
                pageNavNext=b
                carousel.$index[0].innerHTML = arr.ITEMS[i].NAME;
                carousel.$doubleIndex[0].innerHTML = " "
            }else  if( i === 0){

                i++
                b = i + 1
                pageNav = i
                pageNavNext=b
                carousel.$index[0].innerHTML = arr.ITEMS[i].NAME;
                carousel.$doubleIndex[0].innerHTML = arr.ITEMS[b].NAME
            }else if(arr.ITEMS.length-1 > i ){
                i=arr.ITEMS.length-1
                b = arr.ITEMS.length-1
                pageNav = i
                pageNavNext=b
                carousel.$index[0].innerHTML = arr.ITEMS[i].NAME;
                carousel.$doubleIndex[0].innerHTML = " "
            }else if(arr.ITEMS.length-2> i > 0) {
                i = i + 2;
                b = i + 1
                pageNav = i
                pageNavNext=b
                carousel.$index[0].innerHTML = arr.ITEMS[i].NAME;
                carousel.$doubleIndex[0].innerHTML = arr.ITEMS[b].NAME

            }


        }
        if (carousel.prevPage > carousel.pageIndex) {
            i = i - 2
            b = i + 1
            pageNav = i
            pageNavNext=b
            if (i < 0) {
                i = 0;
                pageNav = i
                pageNavNext=b
                carousel.$index[0].innerHTML = arr.ITEMS[i].NAME;
                carousel.$doubleIndex[0].innerHTML = " ";


            } else {
                carousel.$index[0].innerHTML = arr.ITEMS[i].NAME;
                carousel.$doubleIndex[0].innerHTML = arr.ITEMS[b].NAME


            }
        }
        if (ev === event){
            pageNav = i
            pageNavNext=b
            carousel.$index[0].innerHTML =  carousel.$index[1].innerHTML;
            carousel.$doubleIndex[0].innerHTML = carousel.$doubleIndex[1].innerHTML

        }

    }else if(arr.ITEMS.length%2!=0){

        if (ev===event){
            pageNav = i
            pageNavNext=b
            carousel.$index[0].innerHTML =  carousel.$index[1].innerHTML;
            carousel.$doubleIndex[0].innerHTML = carousel.$doubleIndex[1].innerHTML

        } else if (carousel.prevPage < carousel.pageIndex) {


            if(arr.ITEMS.length-2 === i){
                i= arr.ITEMS.length-2
                b =  arr.ITEMS.length-1
                pageNav = i
                pageNavNext=b
                carousel.$index[0].innerHTML = arr.ITEMS[i].NAME;
                carousel.$doubleIndex[0].innerHTML = arr.ITEMS[b].NAME




            } else  if(arr.ITEMS.length-1 === i ) {
                i=arr.ITEMS.length-2
                b = arr.ITEMS.length-1
                pageNav = i
                pageNavNext=b
                carousel.$index[0].innerHTML = arr.ITEMS[i].NAME;
                carousel.$doubleIndex[0].innerHTML = arr.ITEMS[b].NAME

            }else  if( i === 0){

                i++
                b = i + 1
                pageNav = i
                pageNavNext=b
                carousel.$index[0].innerHTML = arr.ITEMS[i].NAME;
                carousel.$doubleIndex[0].innerHTML = arr.ITEMS[b].NAME

            }else if(arr.ITEMS.length-1 > i ){
                i=arr.ITEMS.length-2
                b = arr.ITEMS.length-1
                pageNav = i
                pageNavNext=b
                carousel.$index[0].innerHTML = arr.ITEMS[i].NAME;
                carousel.$doubleIndex[0].innerHTML = arr.ITEMS[b].NAME;

            }else if(arr.ITEMS.length-2> i > 0) {
                i = i + 2;
                b = i + 1
                pageNav = i
                pageNavNext=b
                carousel.$index[0].innerHTML = arr.ITEMS[i].NAME;
                carousel.$doubleIndex[0].innerHTML = arr.ITEMS[b].NAME

            }


        }else if (carousel.prevPage > carousel.pageIndex) {
            i = i - 2
            b = i + 1
            pageNav = i
            pageNavNext=b
            if (i <= 0) {
                i = 0;
                pageNav = i
                pageNavNext=b
                carousel.$index[0].innerHTML = arr.ITEMS[i].NAME;
                carousel.$doubleIndex[0].innerHTML = " ";


            } else {
                carousel.$index[0].innerHTML = arr.ITEMS[i].NAME;
                carousel.$doubleIndex[0].innerHTML = arr.ITEMS[b].NAME


            }
        }
    }
    ev=event

}

function changePageMobile(carousel,event){
    if (ev < event) {

            i = i + 1;
            carousel.$index[0].innerHTML = arr.ITEMS[i].NAME;


    }
    if (ev > event) {
        i = i - 1
        if (i < 0) {
            i = 0;
            carousel.$index[0].innerHTML = arr.ITEMS[i].NAME;
        } else {
            carousel.$index[0].innerHTML = arr.ITEMS[i].NAME;
        }
    }
    ev = event

}

GetApi(magazine);

